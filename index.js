function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	//methods
	this.tackle = function(target) {
		console.log(this.name + " tackled " + target.name);

		target.health -= this.attack;
		if (target.health < 0) {
			target.health = 0;
		}

		console.log(target.name + "'s health is now reduced to " + (target.health));

		if (target.health < 5) {
			target.faint();
		}
	};
	this.faint = function() {
		console.log(this.name + " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let charizard = new Pokemon("Charizard", 8);
console.log(pikachu);
console.log(charizard);

// pikachu.tackle(charizard);
// pikachu.tackle(charizard);

// console.log(charizard);